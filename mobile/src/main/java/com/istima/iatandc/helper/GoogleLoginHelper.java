package com.istima.iatandc.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.plus.model.people.Person;
import com.istima.iatandc.IATAContract;
import com.istima.iatandc.IATAContract.SharedPreferencesEntry;
import com.istima.iatandc.requests.StringVolleyRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.istima.iatandc.helper.MainHelper.isDummyOrNull;


public class GoogleLoginHelper {
    public static GoogleLoginHelper mInstance;
    private Context mContext;
    Response.Listener<String> listener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {


            SharedPreferencesHelper.recordPrefs(mContext, "sync_last_updated", 0L);

            if (SharedPreferencesHelper.getPrefs(mContext).getBoolean("login_google_error", false) && SharedPreferencesHelper.getPrefs(mContext).getBoolean("login_facebook_error", false)) {

            } else {
                SharedPreferencesHelper.recordPrefs(mContext, "login_google_error", false);
            }
        }
    };

    public GoogleLoginHelper(Context context) {
        this.mContext = context.getApplicationContext();
    }

    public synchronized static GoogleLoginHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new GoogleLoginHelper(context.getApplicationContext());
        }
        return mInstance;
    }

    public void clearPreferences() {
        setPreferences("", "", "");
        setAvatarAndName("", "");
        setAuthorized(0);
    }

    public void setPreferences(String userId, String token, String email) {
        Editor prefsEditor = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE).edit();
        prefsEditor.putString(SharedPreferencesEntry.gUserIdKey, userId);
        prefsEditor.putString(SharedPreferencesEntry.gTokenKey, token);
        prefsEditor.putString(SharedPreferencesEntry.gUserEmailKey, email);
        if (userId.isEmpty()) {
            prefsEditor.putInt(SharedPreferencesEntry.gIsAuthorizedKey, 0);
        } else {
            prefsEditor.putInt(SharedPreferencesEntry.gIsAuthorizedKey, 1);
        }
        prefsEditor.apply();

    }

    public void setAccessToken(String token) {
        Editor prefsEditor = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE).edit();
        prefsEditor.putString(SharedPreferencesEntry.gTokenKey, token);
        prefsEditor.commit();

    }

    public void setAvatarAndName(String avatar, String name) {
        Editor prefsEditor = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE).edit();
        prefsEditor.putString(SharedPreferencesEntry.gUsernameKey, name);
        prefsEditor.putString(SharedPreferencesEntry.gAvatarKey, avatar);
        prefsEditor.commit();
    }

    public void setAuthorized(int isLoggedIn) {
        Editor prefsEditor = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE).edit();
        prefsEditor.putInt(SharedPreferencesEntry.gIsAuthorizedKey, isLoggedIn);
        prefsEditor.commit();
    }

    public void setProfile(Person person) {
        if (person != null) {
            if (person.hasImage())
                setAvatar(person.getImage().getUrl());
            if (person.hasName())
                setName(person.getName().getGivenName() + " " + person.getName().getFamilyName());
            if (person.hasId())
                setId(person.getId());
            setAuthorized(1);
        }
    }

    public void setProfile(final GoogleSignInAccount person) {
        if (person != null) {
            if (person.getPhotoUrl() != null)
                setAvatar(person.getPhotoUrl().toString());
            if (person.getDisplayName() != null)
                setName(person.getDisplayName());
            if (person.getId() != null)
                setId(person.getId());
            if (person.getEmail() != null)
                setEmail(person.getEmail());
            MainHelper.log("testlogin", person.getServerAuthCode());
            SharedPreferencesHelper.recordPrefs(mContext, "aurhfaiuhweiuhfiwhe", person.getServerAuthCode());

            setAuthorized(1);
        }
    }

    public String[] getPreferences() {
        String[] prefs = new String[3];
        SharedPreferences myPrefs = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE);
        prefs[0] = myPrefs.getString(SharedPreferencesEntry.gUserIdKey, "");
        prefs[1] = myPrefs.getString(SharedPreferencesEntry.gTokenKey, "");
        return prefs;
    }

    public String getName() {
        SharedPreferences myPrefs = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE);
        return myPrefs.getString(
                SharedPreferencesEntry.gUsernameKey, "Timur A.");
    }

    public void setName(String name) {
        Editor prefsEditor = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE).edit();
        prefsEditor.putString(SharedPreferencesEntry.gUsernameKey, name);
        prefsEditor.commit();
    }

    public String getAvatar() {
        SharedPreferences myPrefs = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE);
        return myPrefs.getString(SharedPreferencesEntry.gAvatarKey, "");
    }

    public void setAvatar(String avatar) {
        Editor prefsEditor = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE).edit();
        prefsEditor.putString(SharedPreferencesEntry.gAvatarKey, avatar);
        prefsEditor.commit();
    }

    public String getEmail() {
        SharedPreferences myPrefs = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE);
        return myPrefs.getString(SharedPreferencesEntry.gUserEmailKey, "") + " "
                + myPrefs.getString(SharedPreferencesEntry.gUserLastNameKey, "");
    }

    public void setEmail(String email) {
        Editor prefsEditor = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE).edit();
        prefsEditor.putString(SharedPreferencesEntry.gUserEmailKey, email);
        prefsEditor.commit();
    }

    public String getId() {
        SharedPreferences myPrefs = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE);
        return myPrefs.getString(SharedPreferencesEntry.gUserIdKey, "");
    }

    public void setId(String id) {
        Editor prefsEditor = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE).edit();
        prefsEditor.putString(SharedPreferencesEntry.gUserIdKey, id);
        prefsEditor.commit();
    }

    public String getToken() {
        SharedPreferences myPrefs = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE);
        return myPrefs.getString(SharedPreferencesEntry.gTokenKey, "");
    }

    public int isAuthorized() {
        SharedPreferences myPrefs = mContext.getSharedPreferences(
                SharedPreferencesEntry.iataPrefKey, Context.MODE_PRIVATE);
        return myPrefs.getInt(SharedPreferencesEntry.gIsAuthorizedKey, 0);
    }

}
