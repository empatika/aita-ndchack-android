package com.istima.iatandc.activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.istima.iatandc.IATAContract;
import com.istima.iatandc.R;
import com.istima.iatandc.helper.GoogleLoginHelper;
import com.istima.iatandc.helper.MainHelper;
import com.istima.iatandc.helper.SharedPreferencesHelper;
import com.istima.iatandc.helper.VolleyQueueHelper;
import com.istima.iatandc.requests.StringVolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FlightActivity extends AppCompatActivity {


    private String id;
    private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_flight);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        btn = (Button)findViewById(R.id.wifi_service_purchase);

        TextView depCode = (TextView)findViewById(R.id.dep_code);
        TextView depTime = (TextView)findViewById(R.id.dep_time);

        TextView date = (TextView)findViewById(R.id.date);
        TextView airline = (TextView)findViewById(R.id.airline_code);

        TextView depTerm = (TextView)findViewById(R.id.dep_terminal);
        TextView arrCode = (TextView)findViewById(R.id.arrival_code);
        TextView arrTime = (TextView)findViewById(R.id.arrival_time);
        TextView arrTerm = (TextView)findViewById(R.id.arrival_terminal);
        try {
            JSONObject jsonObject = new JSONObject(SharedPreferencesHelper.getPrefs(this).getString("flight",""));
            jsonObject = jsonObject.optJSONObject("trip");
            MainHelper.log("teste",jsonObject.toString());
            if(jsonObject.has("wifi")){
                btn.setText(jsonObject.optString("wifi"));
                btn.setClickable(false);
            }
            date.setText(getDate(jsonObject.optJSONArray("flights").getJSONObject(0).optLong("departureTime")));
            airline.setText(String.format("%s %s",jsonObject.optJSONArray("flights").getJSONObject(0).optString("carrier"), jsonObject.optJSONArray("flights").getJSONObject(0).optString("number")));
            depCode.setText(jsonObject.optJSONArray("flights").getJSONObject(0).optString("departureCode"));
            depTime.setText(getTime(jsonObject.optJSONArray("flights").getJSONObject(0).optLong("departureTime")));
            arrCode.setText(jsonObject.optJSONArray("flights").getJSONObject(0).optString("arrivalCode"));
            collapsingToolbar.setTitle(GoogleLoginHelper.getInstance(this).getName());
            arrTime.setText(getTime(jsonObject.optJSONArray("flights").getJSONObject(0).optLong("arrivalTime")));
            id = jsonObject.optString("id");
            MainHelper.log("id id id"+id);

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {final ProgressDialog pd = new ProgressDialog(v.getContext(), ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    pd.show();
                    VolleyQueueHelper.getInstance(FlightActivity.this).addRequest(new StringVolleyRequest(
                            Request.Method.GET, v.getContext(), IATAContract.REQUEST_PREFIX + "api/sita/wifi?trip=" + id, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pd.dismiss();
                            MainHelper.log(response);
                            SharedPreferencesHelper.recordPrefs(FlightActivity.this, "flight", response.toString());
                            try {
                                btn.setText(new JSONObject(response).optJSONObject("trip").optString("wifi"));
                                btn.setClickable(false);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pd.dismiss();
                            Toast.makeText(FlightActivity.this,"Error! Try again", Toast.LENGTH_SHORT).show();

                        }
                    }));
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private String getTime(long datems){
        Date date = new Date(datems*1000L);
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String dateFormatted = formatter.format(date);
        return dateFormatted;
    }

    private String getDate(long datems){
        Date date = new Date(datems*1000L);
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String dateFormatted = formatter.format(date);
        return dateFormatted;
    }
}
