package com.istima.iatandc.requests;

import android.content.Context;
import android.os.Build;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.istima.iatandc.BuildConfig;
import com.istima.iatandc.IATAContract;
import com.istima.iatandc.helper.MainHelper;
import com.istima.iatandc.helper.SharedPreferencesHelper;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Tim on 30.07.2014.
 */
public class StringVolleyRequest extends StringRequest {
    private Context mContext;

    public StringVolleyRequest(int method, Context context, String url, Response.Listener<String> listener,
                               Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.mContext = context;
        setRetryPolicy(new DefaultRetryPolicy(55000, 1, 1));
    }


    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<>();
        params.put("Device-Name", getDeviceName());
        params.put("Device-Language", Locale.getDefault().getLanguage());
        params.put("User-Id", SharedPreferencesHelper.getPrefs(mContext).getString(IATAContract.SharedPreferencesEntry.iataUserIdKey,""));
        MainHelper.log("User-Id", SharedPreferencesHelper.getPrefs(mContext).getString(IATAContract.SharedPreferencesEntry.iataUserIdKey,""));

        params.put("User-agent", String.format("IATA Android/%s", BuildConfig.VERSION_NAME));
        return params;
    }
}
