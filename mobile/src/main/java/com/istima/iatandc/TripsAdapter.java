package com.istima.iatandc;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Tim on 28-May-16.
 */
public class TripsAdapter extends
        RecyclerView.Adapter<TripsAdapter.ViewHolder> {


    private final JSONArray flights;
    private OnFlightClickListener listener;
    private final Context context;
    private ImageLoader imageLoader;

    public TripsAdapter(Context context, JSONArray flights, OnFlightClickListener listener) {
        this.context = context;
        this.flights = flights;
        this.listener = listener;
        imageLoader = ImageLoader.getInstance();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_ticket, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindTrip(new Trip(flights.optJSONObject(position)));
    }

    @Override
    public int getItemCount() {
        return flights.length();
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView departureTextView;
        public TextView departureDateTextView;
        public TextView arrivalDateTextView;
        public TextView arrivalTextView;
        public LinearLayout listView;
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView airlineTextView;
        public TextView priceTextView;
        public TextView messageButton;
        public ImageView pic1;
        public ImageView pic2;
        public ImageView pic3;
        public ImageView pic4;
        public ImageView pic5;
        public ImageView pic6;
        public ImageView pic7;
        public ImageView pic8;


        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null)
                        listener.onFlightClick(new Trip(flights.optJSONObject(getAdapterPosition())));
                }
            });
            airlineTextView = (TextView) itemView.findViewById(R.id.airline_name);
            priceTextView = (TextView) itemView.findViewById(R.id.price);
            departureTextView = (TextView) itemView.findViewById(R.id.departure_airport);
            departureDateTextView = (TextView) itemView.findViewById(R.id.departure_time);
            arrivalTextView = (TextView) itemView.findViewById(R.id.arrival_airport);
            arrivalDateTextView = (TextView) itemView.findViewById(R.id.arrival_time);
            messageButton = (Button) itemView.findViewById(R.id.buyButton);
            pic1 = (ImageView) itemView.findViewById(R.id.pic1);
            pic2 = (ImageView) itemView.findViewById(R.id.pic2);
            pic3 = (ImageView) itemView.findViewById(R.id.pic3);
            pic4 = (ImageView) itemView.findViewById(R.id.pic4);
            pic5 = (ImageView) itemView.findViewById(R.id.pic5);
            pic6 = (ImageView) itemView.findViewById(R.id.pic6);
            pic7 = (ImageView) itemView.findViewById(R.id.pic7);
            pic8 = (ImageView) itemView.findViewById(R.id.pic8);
            listView = (LinearLayout) itemView.findViewById(R.id.linearlist);
        }
        private String getTime(long datems){
            Date date = new Date(datems*1000L);
            DateFormat formatter = new SimpleDateFormat("HH:mm");
            String dateFormatted = formatter.format(date);
            return dateFormatted;
        }
        private void setVisible(View view){
            view.setVisibility(View.VISIBLE);
        }
          private void setGone(View view){
            view.setVisibility(View.GONE);
        }
         public void bindTrip(Trip trip){
             airlineTextView.setText(String.format("%s %s, %s", trip.getAirlineName(), trip.getAirlineCode(), trip.getNumber()));
             departureTextView.setText(String.format("%s, %s airport", trip.getDepartureCode(), trip.getDepartureName()));
             departureDateTextView.setText(getTime(trip.getDepartureDate()));
             arrivalTextView.setText(String.format("%s, %s airport",  trip.getArrivalCode(), trip.getArrivalName()));
             arrivalDateTextView.setText(getTime(trip.getArrivalDate()));
             priceTextView.setText(String.format("\u20AC %s", trip.getPrice()));
             if(!trip.getService_class().equals("Economy")){
                 setVisible(pic7);
                 setVisible(pic8);
             }else{
                 setGone(pic7);
                 setGone(pic8);
             }
             if(trip.getServices()!=null&&trip.getServices().size()>0) {
                 String s = trip.getServices().toString();
                 if (s.contains("lounge_pass")) {
                     setVisible(pic1);
                 } else {
                     setGone(pic1);
                 }

                 if (s.contains("seat_selection")) {
                     setVisible(pic2);
                 } else {
                     setGone(pic2);
                 }
                 if (s.contains("bag_1")) {
                     setVisible(pic3);
                 } else {
                     setGone(pic3);
                 }
                 if (s.contains("bag_2")) {
                     setVisible(pic4);
                 } else {
                     setGone(pic4);
                 }
                 if (s.contains("inflight_wifi")) {
                     setVisible(pic6);
                 } else {
                     setGone(pic6);
                 }
             }
         }
    }

    public interface OnFlightClickListener {
        void onFlightClick(Trip flight);
    }
}