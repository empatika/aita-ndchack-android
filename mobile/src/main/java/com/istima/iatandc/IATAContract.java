package com.istima.iatandc;

public final class IATAContract {

    public static final String TEXT_TYPE = " TEXT";
    public static final String INT_TYPE = " INTEGER";
    public static final String REAL_TYPE = " REAL";
    public static final String COMMA_SEP = ", ";
    public static final String AS_SEP = " as ";
    public static final String DOT_SEP = ".";
    public static final int REQUEST_TIMEOUT = 15000;
    public static String REQUEST_PREFIX = "https://aita-ndchack.herokuapp.com/";

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public IATAContract() {
    }

    /* Inner class that defines the table contents */
    public static abstract class SharedPreferencesEntry {
        public static final String iataUserIdKey = "iata_user_id";
        public static final String iataPrefKey = "iata";
        public static final String gUserIdKey = "g_id";
        public static final String gUserFirstNameKey = "g_first_name";
        public static final String gUserLastNameKey = "g_last_name";
        public static final String gUserEmailKey = "g_email";
        public static final String gTokenKey = "g_token";
        public static final String gIsAuthorizedKey = "g_is_authorized";
        public static final String gUsernameKey = "g_username";
        public static final String gAvatarKey = "g_avatar";
        public static String iataUserKey = "iata_user";
    }

}
