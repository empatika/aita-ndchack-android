package com.istima.iatandc.activity;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.istima.iatandc.helper.GoogleLoginHelper;
import com.istima.iatandc.helper.MainHelper;
import com.istima.iatandc.R;

/**
 * A base class to wrap communication with the Google Play ServicesGoogleApiClient.
 */
public abstract class PlusBaseActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = PlusBaseActivity.class.getSimpleName();

    // A magic number we will use to know that our sign-in error resolution activity has completed
    private static final int OUR_REQUEST_CODE = 49404;
    private static final int RC_SIGN_IN = 21432;
    // A flag to track when a connection is already in progress
    public boolean mGoogleApiClientIsConnecting = false;
    // This is the helper object that connects to Google Play Service.
    protected GoogleApiClient mGoogleSignInApiClient;
    protected GoogleSignInOptions gso;
    // A flag to stop multiple dialogues appearing for the user
    private boolean mAutoResolveOnFail;
    // The saved result from {@link #onConnectionFailed(ConnectionResult)}.  If a connection
    // attempt has been made, this is non-null.
    // If this IS null, then the connect method is still running.
    private ConnectionResult mConnectionResult;
    private GoogleLoginHelper gHelper;
    private boolean mPlusClientIsConnecting;

    /**
     * Called when the {@link GoogleApiClient} revokes access to this app.
     */
    protected abstract void onPlusClientRevokeAccess();

    /**
     * Called when the GoogleApiClientis successfully connected.
     */
    protected abstract void onPlusClientSignIn();

    /**
     * Called when the {@link GoogleApiClient} is disconnected.
     */
    protected abstract void onPlusClientSignOut();

    /**
     * Called when the {@linkGoogleApiClient} is blocking the UI.  If you have a progress bar widget,
     * this tells you when to show or hide it.
     */
    protected abstract void onPlusClientBlockingUI(boolean show);

    /**
     * Called when there is a change in connection state.  If you have "Sign in"/ "Connect",
     * "Sign out"/ "Disconnect", or "Revoke access" buttons, this lets you know when their states
     * need to be updated.
     */
    protected abstract void updateConnectButtonState();

    /**
     * Called when sign in failed.
     *
     * @param reason - reason of failure
     */
    protected void onPlusSignInFailed(String reason) {
        MainHelper.log("testlogin",reason);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String key = "1003804154726-vdqt1baa7tiraknveg82uhg3funvs58i.apps.googleusercontent.com";
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestServerAuthCode(key)
                .requestId()
                .requestIdToken(key)
                .requestScopes(new Scope("https://www.googleapis.com/auth/calendar.readonly"),
                        new Scope("https://mail.google.com/"))
                .build();

// Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        mGoogleSignInApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        gHelper = GoogleLoginHelper.getInstance(this);
    }

    /**
     * Try to sign in the user.
     */
    public void signIn() {
        if (!mGoogleSignInApiClient.isConnected()) {
            setProgressBarVisible(true);
            mAutoResolveOnFail = true;

            if (mConnectionResult != null) {
                startResolution();
            } else {
                initiatePlusClientConnect();
            }
        } else {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
            updateConnectButtonState();

        }
    }

    /**
     * Check if the device supports Google Play Service.  It's best
     * practice to check first rather than handling this as an error case.
     *
     * @return whether the device supports Google Play Service
     */
    protected final boolean supportsGooglePlayServices() {
        return GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS;
    }

    /**
     * Connect the {@linkGoogleApiClient} only if a connection isn't already in progress.  This will
     * call back to {@link #onConnected(Bundle)} or
     * {@link #onConnectionFailed(com.google.android.gms.common.ConnectionResult)}.
     */
    private void initiatePlusClientConnect() {
        if (!mGoogleSignInApiClient.isConnected() && !mGoogleSignInApiClient.isConnecting()) {
            mGoogleSignInApiClient.connect();
        }
    }

    /**
     * Disconnect the {@linkGoogleApiClient} only if it is connected (otherwise, it can throw an error.)
     * This will call back to {@link #onDisconnected()}.
     */
    private void initiatePlusClientDisconnect() {
        if (mGoogleSignInApiClient.isConnected()) {
            mGoogleSignInApiClient.disconnect();
        }
    }

    /**
     * Sign out the user (so they can switch to another account).
     */
    public void signOut() {

        // We only want to sign out if we're connected.
        if (mGoogleSignInApiClient.isConnected()) {
            // Clear the default account in order to allow the user to potentially choose a
            // different account from the account chooser.
            //    Plus.AccountApi.clearDefaultAccount(mGoogleSignInApiClient);

            Auth.GoogleSignInApi.signOut(mGoogleSignInApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            gHelper.clearPreferences();
                            updateConnectButtonState();
                        }
                    });
            updateConnectButtonState();
        } else {
            initiatePlusClientConnect();
            ViewGroup container = (ViewGroup) findViewById(R.id.snackbar_layout);
            MainHelper.showToastShort(container, R.string.error_tryagain_text);
        }

    }

    /**
     * Revoke Google+ authorization completely.
     */
    public void revokeAccess() {

        if (mGoogleSignInApiClient.isConnected()) {
            // Clear the default account as in the Sign Out.
            //      Plus.AccountApi.clearDefaultAccount(mGoogleSignInApiClient);

            gHelper.clearPreferences();

        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        initiatePlusClientConnect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        initiatePlusClientDisconnect();
    }

    public boolean isPlusClientConnecting() {
        return mPlusClientIsConnecting;
    }

    private void setProgressBarVisible(boolean flag) {
        mPlusClientIsConnecting = flag;
        onPlusClientBlockingUI(flag);
    }

    /**
     * A helper method to flip the mResolveOnFail flag and start the resolution
     * of the ConnectionResult from the failed connect() call.
     */
    private void startResolution() {
        try {
            // Don't start another resolution now until we have a result from the activity we're
            // about to start.
            mAutoResolveOnFail = false;
            // If we can resolve the error, then call start resolution and pass it an integer tag
            // we can use to track.
            // This means that when we get the onActivityResult callback we'll know it's from
            // being started here.
            mConnectionResult.startResolutionForResult(this, OUR_REQUEST_CODE);
        } catch (IntentSender.SendIntentException e) {
            // Any problems, just try to connect() again so we get a new ConnectionResult.
            mConnectionResult = null;
            initiatePlusClientConnect();
        }
    }

    /**
     * An earlier connection failed, and we're now receiving the result of the resolution attempt
     * byGoogleApiClient.
     *
     * @see #onConnectionFailed(ConnectionResult)
     */
    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        updateConnectButtonState();
        MainHelper.log("testlogin","request" + requestCode + " resp " + responseCode);
        if (requestCode == OUR_REQUEST_CODE && responseCode == RESULT_OK) {
            // If we have a successful result, we will want to be able to resolve any further
            // errors, so turn on resolution with our flag.
            MainHelper.log("testlogin","result ok");
            mAutoResolveOnFail = true;
            // If we have a successful result, let's call connect() again. If there are any more
            // errors to resolve we'll get our onConnectionFailed, but if not,
            // we'll get onConnected.
            initiatePlusClientConnect();
        } else if (requestCode == OUR_REQUEST_CODE && responseCode != RESULT_OK) {
            // If we've got an error we can't resolve, we're no longer in the midst of signing
            // in, so we can stop the progress spinner.
            MainHelper.log("testlogin","fail");
            setProgressBarVisible(false);
        } else if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);
            MainHelper.log("testlogin","supersucc");
            if (result.isSuccess()) {
                MainHelper.log("testlogin","supersucc 2");
                GoogleSignInAccount account = result.getSignInAccount();
                if (account != null) {
                    MainHelper.log("testlogin","supersucc3");

                    gHelper.setProfile(account);
                    MainHelper.log("testtokens", account.getIdToken());
                    MainHelper.log("testtokens", account.getServerAuthCode());

                    onPlusClientSignIn();
                } else {
                    onPlusSignInFailed("account is null");
                }
            } else {

                onPlusSignInFailed("result is failure " + result.getStatus().toString());
            }
        }
    }

    /**
     * Successfully connected (called byGoogleApiClient)
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        MainHelper.log("testsign", "connected");
        setProgressBarVisible(false);
        updateConnectButtonState();
        if (GoogleLoginHelper.getInstance(this).isAuthorized() == 0) {
            OptionalPendingResult opr =
                    Auth.GoogleSignInApi.silentSignIn(mGoogleSignInApiClient);
            if (opr.isDone()) {
                MainHelper.log("testsign", "done");

            } else {
                MainHelper.log("testsign", "notdone");
                opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                    @Override
                    public void onResult(@NonNull GoogleSignInResult result) {
                        MainHelper.log("testsigntokens1", result.getStatus() + "");

                        if (result.isSuccess()) {
                            GoogleSignInAccount account = result.getSignInAccount();
                            if (account != null) {
                                gHelper.setProfile(account);
                                MainHelper.log("testsigntokens1", account.getIdToken());
                                MainHelper.log("testsigntokens1", account.getServerAuthCode());
                            }
                        }

                    }
                });

            }
        }
    }

    /**
     * Successfully disconnected (called by GoogleApiClient)
     */
    public void onDisconnected() {
        updateConnectButtonState();
        onPlusClientSignOut();
    }

    /**
     * Connection failed for some reason (called by GoogleApiClient)
     * Try and resolve the result.  Failure here is usually not an indication of a serious error,
     * just that the user's input is needed.
     *
     * @see #onActivityResult(int, int, Intent)
     */
    public void onConnectionFailed(ConnectionResult result) {
        updateConnectButtonState();

        // Most of the time, the connection will fail with a user resolvable result. We can store
        // that in our mConnectionResult property ready to be used when the user clicks the
        // sign-in button.
        if (result.hasResolution()) {
            mConnectionResult = result;
            if (mAutoResolveOnFail) {
                // This is a local helper function that starts the resolution of the problem,
                // which may be showing the user an account chooser or similar.
                startResolution();
            }
        }
    }

    public GoogleApiClient getPlusClient() {
        return mGoogleSignInApiClient;
    }

}
