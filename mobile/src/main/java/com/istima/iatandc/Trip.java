package com.istima.iatandc;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tim on 28-May-16.
 */
public class Trip implements Parcelable {

    private String service_class;
    private String number;
    private String airlineName;
    private String airlineCode;
    private String name;
    private int type;
    private double priceValue;
    private String price;
    private String currency;
    private String id;
    private String departureCode;
    private String departureName;
    private String arrivalCode;
    private String arrivalName;
    private long departureDate;
    private long arrivalDate;
    private ArrayList<Service> services;


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public double getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(double priceValue) {
        this.priceValue = priceValue;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepartureCode() {
        return departureCode;
    }

    public void setDepartureCode(String departureCode) {
        this.departureCode = departureCode;
    }

    public String getDepartureName() {
        return departureName;
    }

    public void setDepartureName(String departureName) {
        this.departureName = departureName;
    }

    public String getArrivalCode() {
        return arrivalCode;
    }

    public void setArrivalCode(String arrivalCode) {
        this.arrivalCode = arrivalCode;
    }

    public String getArrivalName() {
        return arrivalName;
    }

    public void setArrivalName(String arrivalName) {
        this.arrivalName = arrivalName;
    }

    public long getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(long departureDate) {
        this.departureDate = departureDate;
    }

    public long getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(long arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    private JSONObject jsonObject;
    public Trip(JSONObject jsonObject){
        this.jsonObject=jsonObject;
        price= jsonObject.optString("price");
        currency= jsonObject.optString("currency");
        id = jsonObject.optString("id");
        service_class = jsonObject.optString("service_class");
        departureName = jsonObject.optJSONObject("flight").optJSONObject("departure_airport").optString("name");
        departureCode = jsonObject.optJSONObject("flight").optJSONObject("departure_airport").optString("code");
        departureDate = jsonObject.optJSONObject("flight").optLong("departure_date");
        arrivalCode = jsonObject.optJSONObject("flight").optJSONObject("arrival_airport").optString("code");
        arrivalName = jsonObject.optJSONObject("flight").optJSONObject("arrival_airport").optString("name");
        arrivalDate = jsonObject.optJSONObject("flight").optLong("arrival_date");
        airlineCode = jsonObject.optJSONObject("flight").optJSONObject("airline").optString("code");
        airlineName = jsonObject.optJSONObject("flight").optJSONObject("airline").optString("name");
        number = jsonObject.optJSONObject("flight").optString("number");
        if(jsonObject.optJSONArray("services")!=null&&jsonObject.optJSONArray("services").length()>0) {
            services = new ArrayList<>();
            JSONArray svJson = jsonObject.optJSONArray("services");
            for(int i=0; i< svJson.length(); i++){
                services.add(new Service(svJson.optJSONObject(i)));
            }
        }

    }


    protected Trip(Parcel in) {
        number = in.readString();
        airlineName = in.readString();
        airlineCode = in.readString();
        name = in.readString();
        type = in.readInt();
        priceValue = in.readDouble();
        price = in.readString();
        currency = in.readString();
        id = in.readString();
        departureCode = in.readString();
        departureName = in.readString();
        arrivalCode = in.readString();
        arrivalName = in.readString();
        departureDate = in.readLong();
        arrivalDate = in.readLong();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(number);
        dest.writeString(airlineName);
        dest.writeString(airlineCode);
        dest.writeString(name);
        dest.writeInt(type);
        dest.writeDouble(priceValue);
        dest.writeString(price);
        dest.writeString(currency);
        dest.writeString(id);
        dest.writeString(departureCode);
        dest.writeString(departureName);
        dest.writeString(arrivalCode);
        dest.writeString(arrivalName);
        dest.writeLong(departureDate);
        dest.writeLong(arrivalDate);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Trip> CREATOR = new Parcelable.Creator<Trip>() {
        @Override
        public Trip createFromParcel(Parcel in) {
            return new Trip(in);
        }

        @Override
        public Trip[] newArray(int size) {
            return new Trip[size];
        }
    };

    public ArrayList<Service> getServices() {
        return services;
    }

    public void setServices(ArrayList<Service> services) {
        this.services = services;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "number='" + number + '\'' +
                ", airlineName='" + airlineName + '\'' +
                ", airlineCode='" + airlineCode + '\'' +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", priceValue=" + priceValue +
                ", price='" + price + '\'' +
                ", currency='" + currency + '\'' +
                ", id='" + id + '\'' +
                ", departureCode='" + departureCode + '\'' +
                ", departureName='" + departureName + '\'' +
                ", arrivalCode='" + arrivalCode + '\'' +
                ", arrivalName='" + arrivalName + '\'' +
                ", departureDate=" + departureDate +
                ", arrivalDate=" + arrivalDate +
                ", services=" + services +
                ", jsonObject='" + jsonObject + '\'' +
                '}';
    }

    public String getService_class() {
        return service_class;
    }

    public void setService_class(String service_class) {
        this.service_class = service_class;
    }
}