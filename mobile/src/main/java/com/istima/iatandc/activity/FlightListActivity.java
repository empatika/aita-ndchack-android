package com.istima.iatandc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.istima.iatandc.IATAContract;
import com.istima.iatandc.R;
import com.istima.iatandc.Trip;
import com.istima.iatandc.TripsAdapter;
import com.istima.iatandc.helper.MainHelper;
import com.istima.iatandc.helper.SharedPreferencesHelper;
import com.istima.iatandc.helper.VolleyQueueHelper;
import com.istima.iatandc.requests.JsonVolleyRequest;
import com.istima.iatandc.requests.StringVolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FlightListActivity extends AppCompatActivity {
    String responseID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String json = getIntent().getExtras().getString("json");
        try {
            final JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.optJSONArray("offers");
            responseID = jsonObject.getString("response_id");
            RecyclerView rv = (RecyclerView) findViewById(R.id.rv_trips);
            TripsAdapter adapter = new TripsAdapter(this, jsonArray, new TripsAdapter.OnFlightClickListener() {
                @Override
                public void onFlightClick(Trip flight) {
                    final ProgressDialog pd = new ProgressDialog(FlightListActivity.this, ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    pd.show();
                    JSONObject jsonObject1 = new JSONObject();
                    try {
                        jsonObject1.put("request_id", responseID);
                        jsonObject1.put("offer", flight.getJsonObject());
                        MainHelper.log("testreq", jsonObject1.toString());
                        VolleyQueueHelper.getInstance(FlightListActivity.this).addRequest(new JsonVolleyRequest(Request.Method.POST, FlightListActivity.this, IATAContract.REQUEST_PREFIX + "api/ndc/order", jsonObject1, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                MainHelper.log("testresp", response.toString());
                                pd.dismiss();
                                SharedPreferencesHelper.recordPrefs(FlightListActivity.this, "flight", response.toString());
                                startActivity(new Intent(FlightListActivity.this, FlightActivity.class));
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pd.dismiss();
                                Toast.makeText(FlightListActivity.this,"Error! Try again", Toast.LENGTH_SHORT).show();
                            }
                        }));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    MainHelper.log("flights", flight.toString());
                }
            });
            rv.setAdapter(adapter);
            // Set layout manager to position the items
            rv.setLayoutManager(new LinearLayoutManager(this));

        } catch (JSONException e) {
            MainHelper.logException(e);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
}
