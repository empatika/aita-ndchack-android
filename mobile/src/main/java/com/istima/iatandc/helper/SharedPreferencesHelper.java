package com.istima.iatandc.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.istima.iatandc.IATAContract;

import java.util.Set;

public class SharedPreferencesHelper {
    public static void recordPrefs(Context context, String key, boolean value) {
        SharedPreferences.Editor prefsEditor = context.getSharedPreferences(
                IATAContract.SharedPreferencesEntry.iataPrefKey,
                Context.MODE_PRIVATE).edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.apply();
    }


    public static void recordPrefs(Context context, String key, long value) {
        SharedPreferences.Editor prefsEditor = context.getSharedPreferences(
                IATAContract.SharedPreferencesEntry.iataPrefKey,
                Context.MODE_PRIVATE).edit();
        prefsEditor.putLong(key, value);
        prefsEditor.apply();
    }

    public static void recordPrefs(Context context, String key, float value) {
        SharedPreferences.Editor prefsEditor = context.getSharedPreferences(
                IATAContract.SharedPreferencesEntry.iataPrefKey,
                Context.MODE_PRIVATE).edit();
        prefsEditor.putFloat(key, value);
        prefsEditor.apply();
    }

    public static void recordPrefs(Context context, String key, int value) {
        SharedPreferences.Editor prefsEditor = context.getSharedPreferences(
                IATAContract.SharedPreferencesEntry.iataPrefKey,
                Context.MODE_PRIVATE).edit();
        prefsEditor.putInt(key, value);
        prefsEditor.apply();
    }

    public static void recordPrefs(Context context, String key, String value) {
        SharedPreferences.Editor prefsEditor = context.getSharedPreferences(
                IATAContract.SharedPreferencesEntry.iataPrefKey,
                Context.MODE_PRIVATE).edit();
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }

    public static void recordPrefs(Context context, String key, Set<String> values) {
        SharedPreferences.Editor prefsEditor = context.getSharedPreferences(
                IATAContract.SharedPreferencesEntry.iataPrefKey,
                Context.MODE_PRIVATE).edit();
        prefsEditor.putStringSet(key, values);
        prefsEditor.apply();
    }

    public static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(IATAContract.SharedPreferencesEntry.iataPrefKey,
                Context.MODE_PRIVATE);
    }
}
