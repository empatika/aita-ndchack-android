package com.istima.iatandc.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.istima.iatandc.IATAContract;
import com.istima.iatandc.R;
import com.istima.iatandc.helper.GoogleLoginHelper;
import com.istima.iatandc.helper.MainHelper;
import com.istima.iatandc.helper.SharedPreferencesHelper;
import com.istima.iatandc.helper.VolleyQueueHelper;
import com.istima.iatandc.requests.StringVolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.istima.iatandc.helper.MainHelper.isDummyOrNull;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class WelcomeActivity extends PlusBaseActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private View mProgressView;
    private int mYear;
    private int mMonth;
    private int mDay;


    @Override
    protected void onPlusClientRevokeAccess() {

    }

    @Override
    protected void onPlusClientSignIn() {
        //TODO: log in request to server
        if (GoogleLoginHelper.getInstance(mContext).isAuthorized() == 1)
            VolleyQueueHelper.getInstance(mContext).addRequest(new StringVolleyRequest(Request.Method.POST, mContext, String.format("%s%s", IATAContract.REQUEST_PREFIX, "api/user/google"), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    SharedPreferencesHelper.recordPrefs(mContext, IATAContract.SharedPreferencesEntry.iataUserKey, response);
                    MainHelper.log("testlogin", response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("code", SharedPreferencesHelper.getPrefs(mContext).getString("aurhfaiuhweiuhfiwhe", ""));
                    return map;
                }
            });

    }

    @Override
    protected void onPlusClientSignOut() {

    }

    @Override
    protected void onPlusClientBlockingUI(boolean show) {

    }

    @Override
    protected void updateConnectButtonState() {

    }

    private Context mContext;
    private EditText dateEditText;
    private EditText depEditText;
    private EditText arrEditText;
    private Button search;

    private void initSearch() {
        search = (Button) findViewById(R.id.search_button);
        depEditText = (EditText) findViewById(R.id.departure);
        arrEditText = (EditText) findViewById(R.id.arrival);
        dateEditText = (EditText) findViewById(R.id.date);

    }

    private void configureSearchScreen() {
        View.OnClickListener clickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(mContext, android.R.style.Widget_DeviceDefault_DatePicker, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, selectedyear);
                        myCalendar.set(Calendar.MONTH, selectedmonth);
                        myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
                        String myFormat = "dd/MM/yy"; //Change as you need
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
                        dateEditText.setText(sdf.format(myCalendar.getTime()));

                        mDay = selectedday;
                        mMonth = selectedmonth;
                        mYear = selectedyear;
                    }
                }, mYear, mMonth, mDay);
                //mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        };
        dateEditText.setOnClickListener(clickListener);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog pd = new ProgressDialog(mContext, ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
                pd.show();
                VolleyQueueHelper.getInstance(mContext).addRequest(new StringVolleyRequest(Request.Method.GET, mContext, IATAContract.REQUEST_PREFIX + "api/ndc/search?from=ATH&to=MUC&date=2016-05-30", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Intent intent = new Intent(mContext, FlightListActivity.class);

                        intent.putExtra("json", response);
                        startActivity(intent);
                        //overridePendingTransition(R.anim.right_in, R.anim.left_out);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                    }
                }));
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        mContext = this;
        mContentView = findViewById(R.id.welcomescreen_content);
        // Configure Google Sign In
        mProgressView = findViewById(R.id.login_progress);

        findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        getSupportActionBar().hide();
        getIdFromServer();
        initSearch();

        configureSearchScreen();
        if (GoogleLoginHelper.getInstance(mContext).isAuthorized() == 1) {
            mContentView.setVisibility(View.GONE);
            if (SharedPreferencesHelper.getPrefs(mContext).getString("flight", "").isEmpty()) {

                findViewById(R.id.searchscreen_content).setVisibility(View.VISIBLE);
            } else {
                startActivity(new Intent(this, FlightActivity.class));
            }
        } else {

        }
        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
    }

    private void getIdFromServer() {
        if (isDummyOrNull(SharedPreferencesHelper.getPrefs(mContext).getString(IATAContract.SharedPreferencesEntry.iataUserIdKey, "")))

            VolleyQueueHelper.getInstance(this).addRequest(new StringVolleyRequest(Request.Method.POST, this, IATAContract.REQUEST_PREFIX + "api/user/register", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.optString("id", "").isEmpty()) {
                            finish();
                        } else {
                            SharedPreferencesHelper.recordPrefs(mContext, IATAContract.SharedPreferencesEntry.iataUserIdKey, jsonObject.optString("id", ""));
                        }
                    } catch (JSONException e) {
                        MainHelper.logException(e);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }));
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        try {
            int shortAnimTime = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } catch (Exception e) {
            MainHelper.logException(e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
