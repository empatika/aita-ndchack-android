package com.istima.iatandc.helper;

import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

/**
 * Created by Tim on 28-May-16.
 */
public class MainHelper {

    public static void showToastShort(View view, int textId) {
        Snackbar.make(view, textId, Snackbar.LENGTH_SHORT).show();
    }

    public static void showToastShort(View view, String text) {
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show();
    }

    public static void logException(Exception e) {
        log(e.getMessage());
    }

    public static void log(String message) {
        log("defaulttag", message);
    }

    public static void log(String tag, String message) {
        Log.e(tag, message);
    }

    public static boolean isDummyOrNull(String str, String dummyPrefix) {
        return str == null ||
                str.isEmpty() ||
                str.toLowerCase().equals("null") ||
                str.startsWith(dummyPrefix) ||
                str.startsWith("Unknown") ||
                str.startsWith("Неизвест");
    }

    public static boolean isDummyOrNull(String str) {
        return isDummyOrNull(str, "234356789");
    }

}
